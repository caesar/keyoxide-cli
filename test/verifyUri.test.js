const spawn = require('child_process').spawn
const chai = require('chai')
const stripAnsi = require('strip-ansi')

const expectedStdout = [
  'PGP key fingerprint: 3637202523e7c1309ab79e99ef2dc5827b445f4b',
  'Verification results:',
  'Yarmo Mackenbach (material for test frameworks) <test@doip.rocks>',
  '  ✓ doip.rocks (dns)',
]

describe('verify', () => {
  it('should verify a valid HKP URI (email address)', () => {
    return new Promise((resolve, reject) => {
      let lines = []
      const proc = spawn('node', ['./src', 'verify', 'hkp:test@doip.rocks'])
      proc.stdout.on('data', (data) => {
        stripAnsi(data.toString())
          .split('\n')
          .forEach((line, i) => {
            if (line !== '') {
              lines.push(line)
            }
          })
      })
      proc.on('close', (code) => {
        if (code != 0) {
          reject(new Error('Exit code was not 0'))
        }
        expectedStdout.forEach((expectedline, i) => {
          if (expectedline != lines[i]) {
            reject(new Error('Unexpected output'))
          }
        })

        resolve()
      })
    })
  }).timeout('12s')
  it('should verify a valid HKP URI (email address + keyserver)', () => {
    return new Promise((resolve, reject) => {
      let lines = []
      const proc = spawn('node', [
        './src',
        'verify',
        'hkp:keyserver.ubuntu.com:test@doip.rocks',
      ])
      proc.stdout.on('data', (data) => {
        stripAnsi(data.toString())
          .split('\n')
          .forEach((line, i) => {
            if (line !== '') {
              lines.push(line)
            }
          })
      })
      proc.on('close', (code) => {
        if (code != 0) {
          reject(new Error('Exit code was not 0'))
        }
        expectedStdout.forEach((expectedline, i) => {
          if (expectedline != lines[i]) {
            reject(new Error('Unexpected output'))
          }
        })

        resolve()
      })
    })
  }).timeout('12s')
  it('should verify a valid HKP URI (fingerprint)', () => {
    return new Promise((resolve, reject) => {
      let lines = []
      const proc = spawn('node', [
        './src',
        'verify',
        'hkp:3637202523e7c1309ab79e99ef2dc5827b445f4b',
      ])
      proc.stdout.on('data', (data) => {
        stripAnsi(data.toString())
          .split('\n')
          .forEach((line, i) => {
            if (line !== '') {
              lines.push(line)
            }
          })
      })
      proc.on('close', (code) => {
        if (code != 0) {
          reject(new Error('Exit code was not 0'))
        }
        expectedStdout.forEach((expectedline, i) => {
          if (expectedline != lines[i]) {
            reject(new Error('Unexpected output'))
          }
        })

        resolve()
      })
    })
  }).timeout('12s')
  it('should verify a valid WKD URI', () => {
    return new Promise((resolve, reject) => {
      let lines = []
      const proc = spawn('node', ['./src', 'verify', 'wkd:test@doip.rocks'])
      proc.stdout.on('data', (data) => {
        stripAnsi(data.toString())
          .split('\n')
          .forEach((line, i) => {
            if (line !== '') {
              lines.push(line)
            }
          })
      })
      proc.on('close', (code) => {
        if (code != 0) {
          reject(new Error('Exit code was not 0'))
        }
        expectedStdout.forEach((expectedline, i) => {
          if (expectedline != lines[i]) {
            reject(new Error('Unexpected output'))
          }
        })

        resolve()
      })
    })
  }).timeout('12s')
  it('should verify a path to a valid key file', () => {
    return new Promise((resolve, reject) => {
      let lines = []
      const proc = spawn('node', ['./src', 'verify', './test/test@doip.rocks.asc'])
      proc.stdout.on('data', (data) => {
        stripAnsi(data.toString())
          .split('\n')
          .forEach((line, i) => {
            if (line !== '') {
              lines.push(line)
            }
          })
      })
      proc.on('close', (code) => {
        if (code != 0) {
          reject(new Error('Exit code was not 0'))
        }
        expectedStdout.forEach((expectedline, i) => {
          if (expectedline != lines[i]) {
            reject(new Error('Unexpected output'))
          }
        })

        resolve()
      })
    })
  }).timeout('12s')
  it('should verify stdin', () => {
    return new Promise((resolve, reject) => {
      let lines = []
      const proc = spawn('node', ['./src', 'verify'], {
        stdio: [spawn('cat', ['./test/test@doip.rocks.asc']).stdout, 'pipe', 'pipe']
      })
      proc.stdout.on('data', (data) => {
        stripAnsi(data.toString())
          .split('\n')
          .forEach((line, i) => {
            if (line !== '') {
              lines.push(line)
            }
          })
      })
      proc.on('close', (code) => {
        if (code != 0) {
          reject(new Error('Exit code was not 0'))
        }
        expectedStdout.forEach((expectedline, i) => {
          if (expectedline != lines[i]) {
            reject(new Error('Unexpected output'))
          }
        })

        resolve()
      })
    })
  }).timeout('12s')
  it('should reject a non-existing HKP URI', () => {
    return new Promise((resolve, reject) => {
      let lines = []
      const proc = spawn('node', [
        './src',
        'verify',
        'hkp:not-a-test@doip.rocks',
      ])
      proc.on('close', (code) => {
        if (code != 1) {
          reject(new Error('Exit code was not 1'))
        }
        resolve()
      })
    })
  })
  it('should reject a non-existing WKD URI', () => {
    return new Promise((resolve, reject) => {
      let lines = []
      const proc = spawn('node', [
        './src',
        'verify',
        'wkd:not-a-test@doip.rocks',
      ])
      proc.on('close', (code) => {
        if (code != 1) {
          reject(new Error('Exit code was not 1'))
        }
        resolve()
      })
    })
  })
  it('should reject an invalid URI', () => {
    return new Promise((resolve, reject) => {
      let lines = []
      const proc = spawn('node', ['./src', 'verify', 'inv:test@doip.rocks'])
      proc.on('close', (code) => {
        if (code != 1) {
          reject(new Error('Exit code was not 1'))
        }
        resolve()
      })
    })
  })
})
