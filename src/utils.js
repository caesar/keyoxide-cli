/*
Copyright (C) 2021 Yarmo Mackenbach

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.

Also add information on how to contact you by electronic and paper mail.

If your software can interact with users remotely through a computer network,
you should also make sure that it provides a way for users to get its source.
For example, if your program is a web application, its interface could display
a "Source" link that leads users to an archive of the code. There are many
ways you could offer source, and different solutions will be better for different
programs; see section 13 for the specific requirements.

You should also get your employer (if you work as a programmer) or school,
if any, to sign a "copyright disclaimer" for the program, if necessary. For
more information on this, and how to apply and follow the GNU AGPL, see <https://www.gnu.org/licenses/>.
*/
const c = require('ansi-colors')

const log = (message, argv) => {
  if (argv.verbose < 0) {
    return
  }
  console.log(message)
}
const log_stdout = (message, argv) => {
  if (argv.verbose < 0) {
    return
  }
  process.stdout.write(message)
}
const info = (message, argv) => {
  if (argv.verbose < 1) {
    return
  }
  console.log(message)
}
const info_stdout = (message, argv) => {
  if (argv.verbose < 1) {
    return
  }
  process.stdout.write(message)
}
const debug = (message, argv) => {
  if (argv.verbose < 2) {
    return
  }
  console.log(message)
}
const debug_stdout = (message, argv) => {
  if (argv.verbose < 2) {
    return
  }
  process.stdout.write(message)
}
const error = (message, argv) => {
  if (argv.verbose < 0) {
    return
  }
  console.error(c.red(message))
  process.exitCode = 1
}

exports.log = log
exports.log_stdout = log_stdout
exports.info = info
exports.info_stdout = info_stdout
exports.debug = debug
exports.debug_stdout = debug_stdout
exports.error = error
