/*
Copyright (C) 2021 Yarmo Mackenbach

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.

Also add information on how to contact you by electronic and paper mail.

If your software can interact with users remotely through a computer network,
you should also make sure that it provides a way for users to get its source.
For example, if your program is a web application, its interface could display
a "Source" link that leads users to an archive of the code. There are many
ways you could offer source, and different solutions will be better for different
programs; see section 13 for the specific requirements.

You should also get your employer (if you work as a programmer) or school,
if any, to sign a "copyright disclaimer" for the program, if necessary. For
more information on this, and how to apply and follow the GNU AGPL, see <https://www.gnu.org/licenses/>.
*/
const fs = require('fs')
const doip = require('doipjs')
const c = require('ansi-colors')
const utils = require('../utils')

const verifyUri = async (argv) => {
  process.exitCode = 1

  let stdin

  if (!argv.uri) {
    try {
      stdin = fs.readFileSync(process.stdin.fd, 'ascii')
    } catch {}
  }

  if (!argv.uri && !stdin) {
    console.log('Please provide a key URI or path, or pipe key data to stdin')
    console.log('Examples:')
    console.log(
      '- keyoxide verify hkp:3637202523e7c1309ab79e99ef2dc5827b445f4b'
    )
    console.log('- keyoxide verify hkp:test@doip.rocks')
    console.log('- keyoxide verify ./key.asc')
    console.log('- gpg --armor --export test@doip.rocks | keyoxide verify')
    return
  }

  let pubKey, keyData

  try {
    utils.info_stdout('- Fetch key...', argv)
    if (fs.existsSync(argv.uri)) {
      pubKey = await doip.keys.fetchPlaintext(fs.readFileSync(argv.uri, 'ascii'))
    } else if (stdin) {
      pubKey = await doip.keys.fetchPlaintext(stdin)
    } else {
      pubKey = await doip.keys.fetchURI(argv.uri)
    }
    utils.info_stdout(' done!\n', argv)
  } catch (e) {
    utils.error(`Error when fetching the key: ${e}`, argv)
    return
  }

  try {
    utils.info_stdout('- Process key...', argv)
    keyData = await doip.keys.process(pubKey)
    utils.info_stdout(' done!\n', argv)
  } catch (e) {
    utils.error(`Error when processing the key: ${e}`, argv)
    return
  }

  utils.log(`PGP key fingerprint: ${keyData.fingerprint}`, argv)

  utils.info_stdout('- Verifying claims...', argv)

  let promises = []
  const doipOpts = {
    proxy: {
      hostname: "proxy.keyoxide.org",
      policy: "adaptive",
    }
  }

  keyData.users.forEach(user => {
    user.claims.forEach(claim => {
      claim.match()
      promises.push(claim.verify(doipOpts))
    })
  })

  await Promise.all(promises)
  .then(values => {
    utils.info_stdout(' done!\n', argv)
  })
  .catch(e => {
    utils.error(`Error when verifying the claims: ${e}`, argv)
  })

  utils.log('Verification results:', argv)

  for (let iUser = 0; iUser < keyData.users.length; iUser++) {
    let index = iUser
    if (index == 0) {
      index = keyData.primaryUserIndex
    } else if (index <= keyData.primaryUserIndex) {
      index--
    }

    utils.log(c.bold(keyData.users[index].userData.id), argv)
    if (keyData.users[index].claims.length == 0) {
      utils.log(`    No claims for this identity`, argv)
    }

    const claimsNoMatchedSP = keyData.users[index].claims.filter(x => x.matches.length == 0);
    const claimsUnordered = keyData.users[index].claims.filter(x => x.matches.length > 0);

    const claimsOrdered = claimsUnordered.sort((a, b) =>
      a.matches[0].serviceprovider.name >
      b.matches[0].serviceprovider.name
        ? 1
        : a.matches[0].serviceprovider.name ===
          b.matches[0].serviceprovider.name
        ? a.matches[0].profile.display >
          b.matches[0].profile.display
          ? 1
          : -1
        : -1
    )

    // Display valid claims
    claimsOrdered.forEach((claim, iClaim) => {
      const prefix = claim.verification.result ? c.green('✓') : c.red('x')
      const serviceproviderName =
        !claim.verification.result && !claim.matches[0].serviceprovider.isAmbiguous
        ? "?"
        : claim.matches[0].serviceprovider.name

      utils.log(
        `  ${prefix} ${claim.matches[0].profile.display} (${serviceproviderName})`,
        argv
      )

      if (argv.v > 0) {
        utils.log(
          `    Profile URL: ${claim.matches[0].profile.uri}`,
          argv
        )
        utils.log(
          `    Proof URL: ${claim.matches[0].proof.uri}`,
          argv
        )
      }
    })

    // Display claims that did not match any service providers
    claimsNoMatchedSP.forEach((claim, iClaim) => {
      if (argv.v > 0) {
        utils.log(
          `  ${c.yellow('!')} ${claim.uri}`,
          argv
        )
        utils.log(
          `    Error: no matched service providers`,
          argv
        )
      }
    })
  }

  process.exitCode = 0
}

exports.verifyUri = verifyUri
