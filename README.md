# keyoxide-cli

A command-line interface written in Node.js to locally verify online distributed
identities, powered by the same method used by
[keyoxide.org](https://keyoxide.org).

## Installation

### Using NPM (preferred)

Simply run:

```
npm install -g keyoxide
```

to install `keyoxide` globally to your system path.

### Using git

Clone this repository, then either run:

```
npm install -g
```

or

```
yarn global add keyoxide
```

to install `keyoxide` globally to your system path.

## Usage

To verify a key stored on dedicated keyservers, run:

```
keyoxide verify hkp:keys.openpgp.org:test@doip.rocks
```

Since `keys.openpgp.org` is the default keyserver, this is equivalent to:

```
keyoxide verify hkp:test@doip.rocks
```

Of course, you can specify any HKP keyserver:

```
keyoxide verify hkp:keyserver.ubuntu.com:test@doip.rocks
```

It is also possible to search for a fingerprint:

```
keyoxide verify hkp:3637202523e7c1309ab79e99ef2dc5827b445f4b
```

To verify a key made accessible through
[Web Key Directory](https://keyoxide.org/guides/web-key-directory), run:

```
keyoxide verify wkd:test@doip.rocks
```

## Community

There's a [Keyoxide Matrix room](https://matrix.to/#/#keyoxide:matrix.org) where
we discuss everything DOIP and Keyoxide.

## Donate

Please consider [donating](https://liberapay.com/Keyoxide/) if you think this
project is a step in the right direction for the internet.

## Funding

This project was realized with funding from
[NLnet](https://nlnet.nl/project/Keyoxide/).
